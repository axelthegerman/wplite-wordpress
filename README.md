# WP Lite - Wordpress

This repository contains the Dockerfile and potentially Development Environment for an opinionated Wordpress site.

These 2 parts might be separated eventually if that makes sense.

## WP Lite Customization

There are 2 categories: SQLite support and hosting related tweaks.

SQLite support is achieved via [wp-sqlite-db](https://github.com/aaemnnosttv/wp-sqlite-db). This is set up automatically in the container.

Hosting related tweaks include software versions (Wordpress, PHP etc.) as well as resource managements (`upload_max_filesize` etc.). These should be configurable via ENV variables so a container restart or (if needed) rebuild applies the new limits.

# Deployment

The goal is to have a management application deploy new apps easily to [Dokku](https://dokku.com) which is able to deploy from Dockerfiles.
Together with [dokku-litestream](https://github.com/AxelTheGerman/dokku-litestream) and other plugins it should provide complete out of the box Wordpress hosting.

Wordpress customizations are ideally deployed via git - either the built-in Dokku deploy functionality or potentially via web hooks.

# Development

Just as important as deployment is the development set up. It should be super easy to run this Docker image locally, develop the Wordpress customizations and push the changes live.

Additional tools will be needed, e.g. to download the live database from the hosted environment.
