ARG PHP_VERSION=8.1
ARG WORDPRESS_VERSION=6.0

FROM wordpress:${WORDPRESS_VERSION}-php${PHP_VERSION}

ARG MAX_UPLOAD_SIZE=50M
ARG WP_SQLITE_DB_VERSION=master

RUN echo "upload_max_filesize = ${MAX_UPLOAD_SIZE}" > $PHP_INI_DIR/conf.d/custom.ini
RUN echo "post_max_size = ${MAX_UPLOAD_SIZE}" >> $PHP_INI_DIR/conf.d/custom.ini

#
# Need to install any WP files in /usr/src/wordpress/
# Any files in /var/www/html will be overridden during the WP install
#

RUN mkdir /usr/src/wordpress/database
RUN mkdir /usr/src/wordpress/wp-content/uploads

# Install SQLite plugin directly from Github
ADD --chown=www-data:www-data https://raw.githubusercontent.com/aaemnnosttv/wp-sqlite-db/$WP_SQLITE_DB_VERSION/src/db.php /usr/src/wordpress/wp-content/

# Copy the default Configuration script to skip set up screen
# TODO: Set proper authentication keys and salts
COPY --chown=www-data:www-data wp-config.php /usr/src/wordpress/
# ADD --chown=www-data:www-data https://gitlab.com/axelthegerman/wplite-wordpress/-/raw/main/wp-config.php /usr/src/wordpress/

COPY --chown=www-data:www-data *plugins /usr/src/wordpress/wp-content/plugins
COPY --chown=www-data:www-data *themes /usr/src/wordpress/wp-content/themes
